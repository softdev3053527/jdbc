/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.softdev.jdbc;

import com.softdev.jdbc.model.User;
import com.softdev.jdbc.service.UserService;

/**
 *
 * @author slmfr
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("user3", "1412");
        if(user != null){
            System.out.println("Welcome "+user.getName());
        }else{
         System.out.println("ERROR");
        }
    }
    
}
