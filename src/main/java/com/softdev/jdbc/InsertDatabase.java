/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.softdev.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author slmfr
 */
public class InsertDatabase {
    public static void main(String[] args) {
        
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";
        try {

            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        } 
        
        String sql = "INSERT INTO product (product_name, product_price, product_size, product_sweet_level, product_type, cat_id) VALUES (?, ?, ?, ?, ?, ?);";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "Iced Coffee");
            stmt.setDouble(2, 4.5);
            stmt.setString(3, "LAR");
            stmt.setString(4, "75");
            stmt.setString(5, "HFC");
            stmt.setInt(6, 1);
            int status =  stmt.executeUpdate();
//            ResultSet key = stmt.getGeneratedKeys();
//            key.next();
//            System.out.println(""+key.getInt(1));

        } catch (SQLException ex) {
            Logger.getLogger(SelectDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        
        
        
        
            if(conn != null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        
    }
}
