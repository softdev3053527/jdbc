/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.softdev.jdbc.dao;


import com.softdev.jdbc.helper.DatabaseHelper;
import com.softdev.jdbc.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author slmfr
 */
public class UserDao implements Dao<User>{

    @Override
    public User get(int id) {
        User  user = null;
        String sql = "SELECT * FROM user WHERE user_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            ResultSet rs = stmt.executeQuery();
            
            while (rs.next()) {
                user = new User();
                user.fromRS(rs);
                
                


                
                
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return  user;
    }
    
        public User getByName(String name) {
        User  user = null;
        String sql = "SELECT * FROM user WHERE user_name=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,name);
            ResultSet rs = stmt.executeQuery();
            
            while (rs.next()) {
                user = new User();
                user.fromRS(rs);
                
                


                
                
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return  user;
    }

    @Override
    public List<User> getAll() {
        ArrayList<User>  list = new ArrayList<>();
        String sql = "SELECT * FROM user";
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                User user = new User();
                user.fromRS(rs);
                
                list.add(user);


                
                
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return  list;
        
    }

    @Override
    public User save(User obj) {
    
        String sql = "INSERT INTO user (user_name,user_password,user_gender,user_role)" + "VALUES(?,?,?,?)";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,obj.getName());
            stmt.setString(2,obj.getPassword());
            stmt.setString(3,obj.getGender());
            stmt.setInt(4,obj.getRole());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
                
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return  obj;
    }

    @Override
    public User update(User obj) {
                String sql = "UPDATE user SET user_name=?,user_password =?,user_gender =?,user_role =? WHERE user_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,obj.getName());
            stmt.setString(2,obj.getPassword());
            stmt.setString(3,obj.getGender());
            stmt.setInt(4,obj.getRole());
            stmt.setInt(5, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
                
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null; 
        }
        
    }

    @Override
    public int delete(User obj) {
                User  user = null;
        String sql = "DELETE FROM user WHERE user_id=?";
        Connection conn = DatabaseHelper.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getId());
            int ret = stmt.executeUpdate();
            return ret;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return  -1;
    }
    public List<User> getAllOrderBy(String name ,String order) {
        ArrayList<User>  list = new ArrayList<>();
        String sql = "SELECT * FROM user ORDER BY " + name + " " + order;
        Connection conn = DatabaseHelper.getConnection();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                User user = new User();
                user.fromRS(rs);
                
                list.add(user);


                
                
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return  list;
        
    }
    
}
