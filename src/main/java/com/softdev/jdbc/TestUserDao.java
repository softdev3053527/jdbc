/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.softdev.jdbc;

import com.softdev.jdbc.dao.UserDao;
import com.softdev.jdbc.helper.DatabaseHelper;
import com.softdev.jdbc.model.User;

/**
 *
 * @author slmfr
 */
public class TestUserDao {
    public static void main(String[] args) {
        
        UserDao userDao = new UserDao();
        for(User u: userDao.getAll()){
            System.out.println(u);
        }
        
//        User user1 = userDao.get(1);
//        System.out.println(user1);
        
//        User newUser = new User("user3","1412",2,"F");
//        User insertedUser = userDao.save(newUser);
//        System.out.println(insertedUser);
//        insertedUser.setGender("M"); 
//          user1.setGender("M");
//          userDao.update(user1);
//          User updateUser = userDao.get(user1.getId());
//          System.out.println(updateUser);
          
//          userDao.delete(user1);
//            userDao.getAllOrderBy("user_name", "asc");
            for(User u: userDao.getAllOrderBy("user_name", "asc")){
                System.out.println(u);
        }
        DatabaseHelper.close();
    }
}
